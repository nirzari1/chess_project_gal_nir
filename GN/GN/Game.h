#pragma once
#include <iostream>
#define BOARD_STR "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"
class Game
{
public:
	char starter;
	void printBoard();
	char** board;
	Game(const char* board = BOARD_STR);
	~Game();
};