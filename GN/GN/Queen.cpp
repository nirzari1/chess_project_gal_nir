#include "Queen.h"
#include "Runner.h"
#include "Zariah.h"

Queen::Queen()
{

}
Queen::~Queen()
{

}
void Queen::move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr)
{
	// doesnt need, we have the zariah and runner
}
bool Queen::isValidMove(const char* coordinates, char* errPtr, bool notMoving) // function to check if the move is valid
{
	Runner check;
	Zariah check1;
	if (check.isValidMove(coordinates, errPtr, notMoving) || check1.isValidMove(coordinates, errPtr, notMoving)) // Checks Zariah And Runner (Zariah movement + Runner movement = Queen movement)
	{
		return true; // valid movement
	}
	return false; // invalid movement
}