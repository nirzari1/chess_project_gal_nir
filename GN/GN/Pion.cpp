#include "Pion.h"

Pion::Pion()
{

}
Pion::~Pion()
{

}
void Pion::move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr)
{
	char currentPiece = 0;
	currentPiece = _var.board[firstNumber][firstLetter];
	_var.board[secondNumber][secondLetter] = currentPiece;
	_var.board[firstNumber][firstLetter] = '#';
	// moves the piece on the board
	char sendToServer = '0'; // valid movement so far, checking for check now
	bool isCheck = false;
	if (currentPiece == 'p') // black piece player played, check for check on white player
	{
		isCheck = chessCheckForWhite();
	}
	else // white piece player played, check for check on black player
	{
		isCheck = chessCheckForBlack();
	}
	if (isCheck) // if there is a check on one of the players
	{
		sendToServer = '1'; // sending valid movement check happened
	}
	*errPtr = sendToServer; // error code equal to the movement message
}
bool Pion::isValidMove(const char* coordinates, char* errPtr, bool notMoving) // function to check if the move is valid
{
	int firstNumber = 0;
	int firstLetter = 0;
	int secondLetter = 0;
	int secondNumber = 0;
	bool err = false;
	char* arrOfValids = new char[6];
	arrOfValids[0] = isInBoard(coordinates); // calls function isInBoard to check if the indexes are in the board
	if (arrOfValids[0] == '0') // if its in the indexes of board
	{
		arrOfValids[1] = noPiece(coordinates, notMoving); // Checks if the source square has a piece or not
		arrOfValids[2] = noPieceOfSamePlayer(coordinates); // Checks if the source and destination have piece of same player
		arrOfValids[3] = isSameSquare(coordinates); // Checks if the chosen destination is the same as source
	}
	if (arrOfValids[0] == '0' && arrOfValids[1] == '0' && arrOfValids[2] == '0' && arrOfValids[3] == '0') // If no errors, keep checking
	{
		int indexesForNumbers = 0;
		int indexesForLetters = 0;
		char temp = coordinates[0] - 97;
		firstLetter = (int)temp;
		firstNumber = (int)coordinates[1] - 48;
		firstNumber = 8 - firstNumber;
		temp = coordinates[2] - 97;
		secondLetter = (int)temp;
		secondNumber = (int)coordinates[3] - 48;
		secondNumber = 8 - secondNumber; // This gets the coordinates on the 2D array (e2e3 = (4, 6), (4, 5))
		if (_var.board[firstNumber][firstLetter] == 'p') // black pion
		{
			if (firstNumber == 1) // first movement of black pion (can move 2 blocks)
			{
				if (!((secondNumber - firstNumber == 1 || secondNumber - firstNumber == 2) && firstLetter == secondLetter)) // not trying to walk forward, checks if wants to eat
				{
					if (!(secondNumber - firstNumber == 1 && secondLetter - firstLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat down right
					{
						if (!(secondNumber - firstNumber == 1 && firstLetter - secondLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat left down aswell, error
						{
							arrOfValids[4] = '6'; // checks all possiblities of first movement of pion, if it got here there's an error
							err = true;
						}
					}
				}
				if (!err)
				{
					arrOfValids[4] = '0'; // valid movement (before checking if wants to move forward 2 and there's someone infront)
				}
				if (firstLetter == secondLetter && !err) // wants to walk forward, needs to check if he's trying to eat walking forward (illegal for pion)
				{
					if (secondNumber - firstNumber == 1) // forward one block
					{
						if (_var.board[secondNumber][secondLetter] != '#')
						{
							arrOfValids[4] = '6'; // trying to eat while walking forward, not valid movement
							err = true;
						}
					}
					else if (secondNumber - firstNumber == 2) // forward two blocks
					{
						if (_var.board[secondNumber][secondLetter] != '#' || _var.board[secondNumber - 1][secondLetter] != '#')
						{
							arrOfValids[4] = '6'; // trying to eat while walking forward/walking forward with a piece in the way, not valid movement
							err = true;
						}
					}
				}
			}
			else // only 1 block movement
			{
				if (!((secondNumber - firstNumber == 1) && firstLetter == secondLetter)) // not trying to move forward, checks if trying to eat..
				{
					if (!(secondNumber - firstNumber == 1 && secondLetter - firstLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat down right
					{
						if (!(secondNumber - firstNumber == 1 && firstLetter - secondLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat left down aswell, error
						{
							arrOfValids[4] = '6';
							err = true; // checked all possiblities of movement of pion, if  the code got here, there's an error in his movement
						}
					}
				}
				if (!err)
				{
					arrOfValids[4] = '0'; // if err didnt change to true, there isnt an error in the movement
				}
				if (firstLetter == secondLetter && !err) // trying to walk forward 1 block, checks if trying to eat as pion
				{
					if (_var.board[secondNumber][secondLetter] != '#')
					{
						arrOfValids[4] = '6'; // tries eating while walking forward, not valid movement
						err = true;
					}
				}
			}
		}
		else if (_var.board[firstNumber][firstLetter] == 'P') // white pion
		{
			if (firstNumber == 6) // first movement of white pion (can move 2 blocks)
			{
				if (!((firstNumber - secondNumber == 2 || firstNumber - secondNumber == 1) && firstLetter == secondLetter)) // not trying to walk forward, checking for diagnole movement
				{
					if (!(firstNumber - secondNumber == 1 && secondLetter - firstLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat up right
					{
						if (!(firstNumber - secondNumber == 1 && firstLetter - secondLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat up left aswell, error
						{
							arrOfValids[4] = '6';
							err = true; //  checked all possiblities of movement of pion, if the code got here, there's an error in the movement
						}
					}
				}
				if (!err)
				{
					arrOfValids[4] = '0'; // valid movement (before checking if wants to move forward 2 and there's someone infront)
				}
				if (secondLetter == firstLetter && !err) // tries walking forward, checks if trying to eat while walking forward..
				{
					if (firstNumber - secondNumber == 1) // forward 1 block
					{
						if (_var.board[secondNumber][secondLetter] != '#')
						{
							arrOfValids[4] = '6'; // trying to eat while walking forward, error
							err = true;
						}
					}
					else if (firstNumber - secondNumber == 2) // forward 2 blocks
					{
						if (_var.board[secondNumber][secondLetter] != '#' || _var.board[secondNumber + 1][secondLetter] != '#')
						{
							arrOfValids[4] = '6'; // trying to eat while walking forward / walk through a piece, error
							err = true;
						}
					}
				}
			}
			else // only 1 block movement
			{
				if (!((firstNumber - secondNumber == 1) && firstLetter == secondLetter)) // not trying to walk forward
				{
					if (!(firstNumber - secondNumber == 1 && secondLetter - firstLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat up right
					{
						if (!(firstNumber - secondNumber == 1 && firstLetter - secondLetter == 1 && _var.board[secondNumber][secondLetter] != '#')) // not trying to eat up left aswell, error
						{
							arrOfValids[4] = '6';
							err = true; // if the code got here, it means there's an error in the pion movement
						}
					}
				}
				if (!err)
				{
					arrOfValids[4] = '0'; // valid movement
				}
				if (secondLetter == firstLetter && !err) // tries walking forward, checks if trying to eat while walking forward
				{
					if (_var.board[secondNumber][secondLetter] != '#')
					{
						arrOfValids[4] = '6'; // tries eating while walking forward as pion, invalid movement
						err = true;
					}
				}
			}
		}
	}
	bool chessOnBlack = false;
	bool chessOnWhite = false;
	if (arrOfValids[4] == '0' && !notMoving) // valid movement, need to check if its chess on himself
	{
		char valueBefore = _var.board[secondNumber][secondLetter];
		if (_var.board[firstNumber][firstLetter] == 'p') // black player moving
		{
			_var.board[secondNumber][secondLetter] = 'p'; // puts in destination pion
			_var.board[firstNumber][firstLetter] = '#'; // empty out the source
			// this imitiates a situation where i have already moved, now need to check if there's check on black player (after his movement)
			chessOnBlack = chessCheckForBlack();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = 'p'; // puts the pieces back in their place
		}
		else // white player moving
		{
			_var.board[secondNumber][secondLetter] = 'P';
			_var.board[firstNumber][firstLetter] = '#'; // puts in destination pion and empty out the source square
			// this imitiates a situatuion where i have already moved, now need to check if there's check on white player
			chessOnWhite = chessCheckForWhite();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = 'P'; // puts pieces back in their place
		}
	}
	if (chessOnWhite || chessOnBlack)
	{
		arrOfValids[5] = '4'; // if one of them is true, means the player did a check on himself.
	}
	else
	{
		arrOfValids[5] = '0'; // the movement does not cause check on the current player
	}
	if (notMoving) // if check is being checked, no point in having it checked on the current piece
	{
		arrOfValids[5] = '0';
	}
	int flag = 0;
	for (int i = 0; i < 6 && !flag; i++)
	{
		if (arrOfValids[i] != '0')
		{
			flag = 1; // checks if there is any invalid movements
			if (!notMoving)
			{
				*errPtr = arrOfValids[i]; // saves the error code
			}
		}
	}
	if (!flag) // if flag stayed 0 it calls move function to move
	{
		if (!notMoving)
		{
			move(firstNumber, secondNumber, firstLetter, secondLetter, errPtr);
		}
		delete[] arrOfValids;
		return true;
	}
	delete[] arrOfValids;
	return false;
}
void Pion::setStarter(char start)
{
	_var.starter = start; // setter for who's next turn is
}
char Pion::getStarter()
{
	return _var.starter; // getter for starter to check who is playing
}
char Pion::getPieceInIndex(int i, int j)
{
	return _var.board[i][j]; // getter for what piece is currently being moved
}