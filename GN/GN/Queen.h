#pragma once
#include "General.h"

class Queen : public Piece
{
public:
	Queen();
	~Queen();
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving = false);
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr);
};