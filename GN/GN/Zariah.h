#pragma once
#include "General.h"

class Zariah : public Piece
{
public:
	Zariah();
	~Zariah();
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving = false);
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr);
};
