#pragma once
#include "General.h"

class Pion : public Piece
{
public:
	Pion();
	~Pion();
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving = false);
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr);
	void setStarter(char start);
	char getStarter();
	char getPieceInIndex(int i, int j);
};