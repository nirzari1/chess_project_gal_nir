/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Zariah.h"
#include "Runner.h"
#include "Queen.h"
#include "King.h"
#include "Parash.h"
#include "Pion.h"

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	Pion pion;
	Queen queen;
	Parash parash;
	King king;
	Runner runner;
	Zariah zariah; // 6 pieces of chess
	char* msg = new char[4];
	char errorCode = '0';
	char* ptrForError = &errorCode;
	int firstNumber = 0;
	int firstLetter = 0;
	char currentPiece = 0;
	bool validMove = false;

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		validMove = false;
		for (int i = 0; i < 4; i++)
		{
			msg[i] = msgFromGraphics[i];
		}
		firstLetter = (int)msg[0] - 97;
		firstNumber = (int)msg[1] - 48;
		firstNumber = 8 - firstNumber; // gets indexes on the 2d array we have
		currentPiece = pion.getPieceInIndex(firstNumber, firstLetter); // gets the current piece, number - i, letter - j
		switch (currentPiece) // switch case to go for the piece that is needed on the board (if moving queen - case q)
		{
			case 'r':
				validMove = zariah.isValidMove(msg, ptrForError);
				break;

			case 'k':
				validMove = king.isValidMove(msg, ptrForError);
				break;

			case 'q':
				validMove = queen.isValidMove(msg, ptrForError);
				break;

			case 'n':
				validMove = parash.isValidMove(msg, ptrForError);
				break;

			case 'b':
				validMove = runner.isValidMove(msg, ptrForError);
				break;

			case 'p':
				validMove = pion.isValidMove(msg, ptrForError);	
				break;

			case 'R':
				validMove = zariah.isValidMove(msg, ptrForError);
				break;

			case 'K':
				validMove = king.isValidMove(msg, ptrForError);
				break;

			case 'Q':
				validMove = queen.isValidMove(msg, ptrForError);
				break;

			case 'N':
				validMove = parash.isValidMove(msg, ptrForError);
				break;

			case 'B':
				validMove = runner.isValidMove(msg, ptrForError);
				break;

			case 'P':
				validMove = pion.isValidMove(msg, ptrForError);
				break;

			case '#':
				errorCode = '2';
				break;

		}
		
		msgToGraphics[0] = errorCode;
		msgToGraphics[1] = 0; // saves the code to send to server
		

		if (validMove) // valid movement was made
		{
			if (pion.getStarter() == '0') // if the white piece player just played
			{
				pion.setStarter('1'); // black piece turn
			}
			else // black piece player played valid movement
			{
				pion.setStarter('0'); // white piece turn
			}
		}
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}