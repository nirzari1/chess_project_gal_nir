#include "Game.h"

Game::Game(const char* copyboard)
{
	int i = 0;
	int j = 0;
	int counter = 0;
	board = new char* [8];
	for (i = 0; i < 8; i++)
	{
		board[i] = new char[8];
	}
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			board[i][j] = copyboard[counter];
			counter++;
		}
	}
	starter = copyboard[64];
}
Game::~Game()
{
	for (int i = 0; i < 8; i++)
	{
		delete[] board[i];
	}
	delete[] board;
}
void Game::printBoard()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			std::cout << board[i][j] << "  ";
		}
		std::cout << std::endl;
	}
	if (starter == '0')
	{
		std::cout << "Starter Player is White Color " << std::endl;
	}
	else
	{
		std::cout << "Starter Player is Black Color " << std::endl;
	}
}