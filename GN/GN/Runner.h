#pragma once
#include "General.h"

class Runner : public Piece
{
public:
	Runner();
	~Runner();
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving = false);
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr);
};