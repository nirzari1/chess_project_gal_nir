#include "Zariah.h"

Zariah::Zariah() // c'tor
{

}
Zariah::~Zariah() // d'tor
{

}
void Zariah::move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr) // function to move the piece
{
	char currentPiece = 0;
	currentPiece = _var.board[firstNumber][firstLetter];
	_var.board[secondNumber][secondLetter] = currentPiece;
	_var.board[firstNumber][firstLetter] = '#';
	// moves the piece on the board
	char sendToServer = '0'; // valid movement so far, checking for check now
	bool isCheck = false;
	if (currentPiece == 'r' || currentPiece == 'q') // black piece player played, check for check on white player
	{
		isCheck = chessCheckForWhite();
	}
	else // white piece player played, check for check on black player
	{
		isCheck = chessCheckForBlack();
	}
	if (isCheck) // if there is a check on one of the players
	{
		sendToServer = '1'; // sending valid movement check happened
	}
	*errPtr = sendToServer; // error code equal to the movement message
}
bool Zariah::isValidMove(const char* coordinates, char* errPtr, bool notMoving) // function to check if the move is valid
{
	int firstNumber = 0;
	int firstLetter = 0;
	int secondNumber = 0;
	int secondLetter = 0;
	char* arrOfValids = new char[6];
	arrOfValids[0] = isInBoard(coordinates); // calls function isInBoard to check if the indexes are in the board
	if (arrOfValids[0] == '0') // if its in the indexes of board
	{
		arrOfValids[1] = noPiece(coordinates, notMoving); // Checks if the source square has a piece or not
		arrOfValids[2] = noPieceOfSamePlayer(coordinates); // Checks if the source and destination have piece of same player
		arrOfValids[3] = isSameSquare(coordinates); // Checks if the chosen destination is the same as source
	}
	if (arrOfValids[0] == '0' && arrOfValids[1] == '0' && arrOfValids[2] == '0' && arrOfValids[3] == '0') // If no errors, keep checking
	{
		if (coordinates[0] == coordinates[2] || coordinates[1] == coordinates[3]) // movement of Zariah
		{
			int indexes = 0;
			char temp = coordinates[0] - 97;
			firstLetter = (int)temp;
			firstNumber = (int)coordinates[1] - 48;
			firstNumber = 8 - firstNumber;
			temp = coordinates[2] - 97;
			secondLetter = (int)temp;
			secondNumber = (int)coordinates[3] - 48;
			secondNumber = 8 - secondNumber; // This gets the coordinates on the 2D array (e2e3 = (4, 6), (4, 5))
			if (coordinates[0] != coordinates[2]) // numbers are equal (move right / left)
			{
				if (firstLetter < secondLetter) // move right
				{
					indexes = firstLetter + 1;
					while (indexes != secondLetter) // Until gets to destination
					{
						if (_var.board[firstNumber][indexes] != '#') // if there is a piece on the way
						{
							arrOfValids[4] = '6'; // error code 6 happened
						}
						indexes++;
					}
					if (arrOfValids[4] != '6') // if the error didnt happen
					{
						arrOfValids[4] = '0'; // Move is valid
					}
				}
				else // move left
				{
					indexes = firstLetter - 1;
					while (indexes != secondLetter) // Until gets to destination
					{
						if (_var.board[firstNumber][indexes] != '#') // if there is a piece on the way
						{
							arrOfValids[4] = '6'; // error code 6 happened
						}
						indexes--;
					}
					if (arrOfValids[4] != '6') // if the error didnt happen
					{
						arrOfValids[4] = '0'; // Move is valid
					}
				}
			}
			else // letters are equal (move up / down)
			{
				if (firstNumber < secondNumber) // move down
				{
					indexes = firstNumber + 1;
					while (indexes != secondNumber) // Until reached destination
					{
						if (_var.board[indexes][firstLetter] != '#') // if there is a piece on the way
						{
							arrOfValids[4] = '6'; // error code 6 happened
						}
						indexes++;
					}
					if (arrOfValids[4] != '6') // If no error happened
					{
						arrOfValids[4] = '0'; // Move is valid
					}
				}
				else // move up
				{
					indexes = firstNumber - 1;
					while (indexes != secondNumber) // Until reached destination
					{
						if (_var.board[indexes][firstLetter] != '#') // if there is a piece on the way
						{
							arrOfValids[4] = '6'; // error code 6 happened
						}
						indexes--;
					}
					if (arrOfValids[4] != '6') // if the error didnt happen
					{
						arrOfValids[4] = '0'; // Valid move
					}
				}
			}
		}
		else // The movement is not valid
		{
			arrOfValids[4] = '6';
		}
	}
	bool chessOnBlack = false;
	bool chessOnWhite = false;
	if (arrOfValids[4] == '0' && !notMoving) // valid movement, need to check if its chess on himself
	{
		char chosenPiece = _var.board[firstNumber][firstLetter];
		char valueBefore = _var.board[secondNumber][secondLetter];
		if (_var.board[firstNumber][firstLetter] == 'r' || _var.board[firstNumber][firstLetter] == 'q') // black player moving
		{
			_var.board[secondNumber][secondLetter] = chosenPiece; // puts in destination pion
			_var.board[firstNumber][firstLetter] = '#'; // empty out the source
			// this imitiates a situation where i have already moved, now need to check if there's check on black player (after his movement)
			chessOnBlack = chessCheckForBlack();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = chosenPiece; // puts the pieces back in their place
		}
		else // white player moving
		{
			_var.board[secondNumber][secondLetter] = chosenPiece;
			_var.board[firstNumber][firstLetter] = '#'; // puts in destination pion and empty out the source square
			// this imitiates a situatuion where i have already moved, now need to check if there's check on white player
			chessOnWhite = chessCheckForWhite();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = chosenPiece; // puts pieces back in their place
		}
	}
	if (chessOnWhite || chessOnBlack)
	{
		arrOfValids[5] = '4'; // if one of them is true, means the player did a check on himself.
	}
	else
	{
		arrOfValids[5] = '0'; // the movement does not cause check on the current player
	}
	if (notMoving) // if check is being checked, no point in having it checked on the current piece
	{
		arrOfValids[5] = '0';
	}
	int flag = 0;
	for (int i = 0; i < 6 && !flag; i++)
	{
		if (arrOfValids[i] != '0')
		{
			flag = 1; // checks if there is any invalid movements
			if (!notMoving)
			{
				*errPtr = arrOfValids[i]; // saves the error code
			}
		}
	}
	if (!flag) // if flag stayed 0 it calls move function to move
	{
		if (!notMoving)
		{
			move(firstNumber, secondNumber, firstLetter, secondLetter, errPtr);
		}
		delete[] arrOfValids;
		return true;
	}
	delete[] arrOfValids;
	return false;
}