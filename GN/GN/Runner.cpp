#include "Runner.h"

Runner::Runner()
{

}
Runner::~Runner()
{

}
void Runner::move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr)
{
	char currentPiece = 0;
	currentPiece = _var.board[firstNumber][firstLetter];
	_var.board[secondNumber][secondLetter] = currentPiece;
	_var.board[firstNumber][firstLetter] = '#';
	// moves the piece on the board
	char sendToServer = '0'; // valid movement so far, checking for check now
	bool isCheck = false;
	if (currentPiece == 'b' || currentPiece == 'q') // black piece player played, check for check on white player (can be runner or queen beacuse queen uses runner)
	{
		isCheck = chessCheckForWhite();
	}
	else // white piece player played, check for check on black player
	{
		isCheck = chessCheckForBlack();
	}
	if (isCheck) // if there is a check on one of the players
	{
		sendToServer = '1'; // sending valid movement check happened
	}
	*errPtr = sendToServer; // error code equal to the movement message
}
bool Runner::isValidMove(const char* coordinates, char* errPtr, bool notMoving) // function to check if the move is valid
{
	int firstNumber = 0;
	int secondNumber = 0;
	int firstLetter = 0;
	int secondLetter = 0;
	char* arrOfValids = new char[6];
	arrOfValids[0] = isInBoard(coordinates); // calls function isInBoard to check if the indexes are in the board
	if (arrOfValids[0] == '0') // if its in the indexes of board
	{
		arrOfValids[1] = noPiece(coordinates, notMoving); // Checks if the source square has a piece or not
		arrOfValids[2] = noPieceOfSamePlayer(coordinates); // Checks if the source and destination have piece of same player
		arrOfValids[3] = isSameSquare(coordinates); // Checks if the chosen destination is the same as source
	}
	if (arrOfValids[0] == '0' && arrOfValids[1] == '0' && arrOfValids[2] == '0' && arrOfValids[3] == '0') // If no errors, keep checking
	{
		int indexesForNumbers = 0;
		int indexesForLetters = 0;
		char temp = coordinates[0] - 97;
		firstLetter = (int)temp;
		firstNumber = (int)coordinates[1] - 48;
		firstNumber = 8 - firstNumber;
		temp = coordinates[2] - 97;
		secondLetter = (int)temp;
		secondNumber = (int)coordinates[3] - 48;
		secondNumber = 8 - secondNumber; // This gets the coordinates on the 2D array (e2e3 = (4, 6), (4, 5))
		// movement of Runner (Numbers in coordinates = i, Letters in coordinates - j [i][j])
		if (secondNumber - firstNumber == secondLetter - firstLetter && secondLetter - firstLetter > 0) // move down right (add x to both letter and number)
		{
			indexesForNumbers = firstNumber + 1;
			indexesForLetters = firstLetter + 1; // moves to the next index on the path
			while (indexesForNumbers != secondNumber && indexesForLetters != secondLetter) // until reached destination
			{
				if (_var.board[indexesForNumbers][indexesForLetters] != '#') // if there's a piece in the way
				{
					arrOfValids[4] = '6'; // error code 6 happened
				}
				indexesForNumbers++;
				indexesForLetters++; // adds 1 to the index to go forward
			}
			if (arrOfValids[4] != '6') // If the error code 6 didn't happen
			{
				arrOfValids[4] = '0'; // valid movement 
			}
		}
		else if (firstNumber - secondNumber == secondLetter - firstLetter && firstNumber - secondNumber > 0) // move up right (add x to firstletter and sub x from firstnumber)
		{
			indexesForNumbers = firstNumber - 1;
			indexesForLetters = firstLetter + 1; // moves to the next index on the path
			while (indexesForNumbers != secondNumber && indexesForLetters != secondLetter) // until reached destination
			{
				if (_var.board[indexesForNumbers][indexesForLetters] != '#') // if there's a piece in the way
				{
					arrOfValids[4] = '6'; // error code 6 happened
				}
				indexesForNumbers--;
				indexesForLetters++; // adds 1 to the index to go forward
			}
			if (arrOfValids[4] != '6') // if the error code 6 didn't happen
			{
				arrOfValids[4] = '0'; // valid movement
			}
		}
		else if (secondNumber - firstNumber == firstLetter - secondLetter) // move down left (add x to firstNumber and sub x from firstLetter)
		{
			indexesForNumbers = firstNumber + 1;
			indexesForLetters = firstLetter - 1; // moves to the next index on the path
			while (indexesForNumbers != secondNumber && indexesForLetters != secondLetter) // until reached destination
			{
				if (_var.board[indexesForNumbers][indexesForLetters] != '#') // if there's a piece in the way
				{
					arrOfValids[4] = '6'; // error code 6 happened
				}
				indexesForNumbers++;
				indexesForLetters--; // adds 1 to the index to go forward
			}
			if (arrOfValids[4] != '6') // if the error code 6 didn't happen
			{
				arrOfValids[4] = '0'; // valid movement
			}
		}
		else if (firstNumber - secondNumber == firstLetter - secondLetter) // move up left (sub x from both letter and number)
		{
			indexesForNumbers = firstNumber - 1;
			indexesForLetters = firstLetter - 1; // moves to the next index on the path
			while (indexesForNumbers != secondNumber && indexesForLetters != secondLetter) // until reached destination
			{
				if (_var.board[indexesForNumbers][indexesForLetters] != '#') // if there's a piece in the way
				{
					arrOfValids[4] = '6'; // error code 6 happened
				}
				indexesForNumbers--;
				indexesForLetters--; // adds 1 to the index to go forward
			}
			if (arrOfValids[4] != '6') // if error code 6 didn't happen
			{
				arrOfValids[4] = '0'; // valid movement
			}
		}
		else // The movement is not valid
		{
			arrOfValids[4] = '6';
		}
	}
	bool chessOnBlack = false;
	bool chessOnWhite = false;
	if (arrOfValids[4] == '0' && !notMoving) // valid movement, need to check if its chess on himself
	{
		char chosenPiece = _var.board[firstNumber][firstLetter];
		char valueBefore = _var.board[secondNumber][secondLetter];
		if (_var.board[firstNumber][firstLetter] == 'b' || _var.board[firstNumber][firstLetter] == 'q') // black player moving
		{
			_var.board[secondNumber][secondLetter] = chosenPiece; // puts in destination 
			_var.board[firstNumber][firstLetter] = '#'; // empty out the source
			// this imitiates a situation where i have already moved, now need to check if there's check on black player (after his movement)
			chessOnBlack = chessCheckForBlack();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = chosenPiece; // puts the pieces back in their place
		}
		else // white player moving
		{
			_var.board[secondNumber][secondLetter] = chosenPiece;
			_var.board[firstNumber][firstLetter] = '#'; // puts in destination pion and empty out the source square
			// this imitiates a situatuion where i have already moved, now need to check if there's check on white player
			chessOnWhite = chessCheckForWhite();
			_var.board[secondNumber][secondLetter] = valueBefore;
			_var.board[firstNumber][firstLetter] = chosenPiece; // puts pieces back in their place
		}
	}
	if (chessOnWhite || chessOnBlack)
	{
		arrOfValids[5] = '4'; // if one of them is true, means the player did a check on himself.
	}
	else
	{
		arrOfValids[5] = '0'; // the movement does not cause check on the current player
	}
	if (notMoving) // if check is being checked, no point in having it checked on the current piece
	{
		arrOfValids[5] = '0';
	}
	int flag = 0;
	for (int i = 0; i < 6 && !flag; i++)
	{
		if (arrOfValids[i] != '0')
		{
			flag = 1; // checks if there is any invalid movements
			if (!notMoving)
			{
				*errPtr = arrOfValids[i]; // saves the error code
			}
		}
	}
	if (!flag) // if flag stayed 0 it calls move function to move
	{
		if (!notMoving)
		{
			move(firstNumber, secondNumber, firstLetter, secondLetter, errPtr);
		}
		delete[] arrOfValids;
		return true;
	}
	delete[] arrOfValids;
	return false;
}