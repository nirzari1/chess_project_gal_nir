#pragma once
#include <iostream>
#include "Game.h"
class Piece
{
protected:
	static Game _var;
	Piece();
	~Piece();
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr) = 0;
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving) = 0;
	char isInBoard(const char* coordinates);
	char isSameSquare(const char* coordinates);
	char noPieceOfSamePlayer(const char* coordinates);
	char noPiece(const char* coordinates, bool checkingCheck);
	bool chessCheckForWhite();
	bool chessCheckForBlack();

};
