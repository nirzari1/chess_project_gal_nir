#pragma once
#include "General.h"

class King : public Piece
{
public:
	King();
	~King();
	virtual bool isValidMove(const char* coordinates, char* errPtr, bool notMoving = false);
	virtual void move(int firstNumber, int secondNumber, int firstLetter, int secondLetter, char* errPtr);
};