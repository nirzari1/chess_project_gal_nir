#include "Pion.h"
#include "Zariah.h"
#include "Queen.h"
#include "King.h"
#include "Runner.h"
#include "Parash.h" // used for check function, general.h gets included too from those files

Game Piece::_var; // initiliaze static var

Piece::Piece()
{

}
Piece::~Piece()
{

}
char Piece::noPiece(const char* coordinates, bool checkingCheck) // this function checks if the source square has no piece on it
{
	char firstLetter = coordinates[0] - 97;
	int itsNumber = (int)firstLetter;
	int secondNumber = (int)coordinates[1] - 48;
	secondNumber = 8 - secondNumber; // gets the [i][j] for the 2D array in order to check the board
	if (_var.board[secondNumber][itsNumber] == '#') // If there is a '#' on source square means no piece is on it
	{
		return '2'; // error code 2 happened
	}
	else if (_var.board[secondNumber][itsNumber] >= 'a' && _var.board[secondNumber][itsNumber] <= 'z' && _var.starter == '0' && !checkingCheck)
	{
		return '2'; // white piece player tried moving black piece
	}
	else if (_var.board[secondNumber][itsNumber] >= 'A' && _var.board[secondNumber][itsNumber] <= 'Z' && _var.starter != '0' && !checkingCheck)
	{
		return '2'; // black piece player tried moving white piece
	}
	return '0'; // valid
}
char Piece::isSameSquare(const char* coordinates) // checks if source and destination are the same
{
	if (coordinates[0] == coordinates[2] && coordinates[1] == coordinates[3]) // first half of string is equal to 2nd half of string
	{
		return '7'; // error code 7 happened
	}
	return '0'; // valid
}
char Piece::isInBoard(const char* coordinates) // checks if the source and destination are inside the board
{
	for (int i = 0; i < 4; i++)
	{
		if (i % 2 == 0 && (!(coordinates[i] >= 'a' && coordinates[i] <= 'h'))) // its a letter (0, 2) - indexes
			// DELETED NESTED IF (CODE REVIEW)
		{
			return '5'; // if the  letter is above the limit or below the limit (h < letter < a), error code 5 happened
		}
		else // its a number (1, 3) - indexes
		{
			if (!(coordinates[i] >= '1' && coordinates[i] <= '8'))
			{
				return '5'; // if the number is below or above the limit (8 < number < 1), error code 5 happened
			}
		}
	}
	return '0'; // valid
}
char Piece::noPieceOfSamePlayer(const char* coordinates) // destination square has piece of same player playing (cant be eaten)
{
	int* arr = new int[4];
	char temp = '0';
	for (int i = 0; i < 4; i++)
	{
		if (i % 2 == 0) // letter
		{
			temp = coordinates[i] - 97;
			arr[i] = (int)temp; // converts the letter to an index number(0-7)
		}
		else // number
		{
			arr[i] = (int)coordinates[i] - 48;
			arr[i] = 8 - arr[i]; // converts the char number to a normal number but makes it opposite (8 = 0, 0 = 8)
		}
	}
	if (_var.board[arr[1]][arr[0]] >= 'a' && _var.board[arr[1]][arr[0]] <= 'z'  && _var.board[arr[3]][arr[2]] >= 'a' && _var.board[arr[3]][arr[2]] <= 'z' && _var.board[arr[1]][arr[0]] != '#') // if they are the  color type
	{
		delete[] arr;
		return '3'; // error code 3 happened
	}
	else if (_var.board[arr[1]][arr[0]] >= 'A' && _var.board[arr[1]][arr[0]] <= 'Z' && _var.board[arr[3]][arr[2]] >= 'A' && _var.board[arr[3]][arr[2]] <= 'Z' && _var.board[arr[1]][arr[0]] != '#') // if they are the same color type
	{
		delete[] arr;
		return '3'; // error code 3 happened
	}
	delete[] arr;
	return '0';
}
bool Piece::chessCheckForWhite() // check if there's check on white player
{
	Pion pion;
	Zariah zariah;
	Queen queen;
	King king;
	Runner runner;
	Parash parash; // used to check valid movements onto the enemy king spot, will be sent a bool true value to not actually move if its a valid movement (bool notMoving variable)
	char kingNumber = 0;
	char kingLetter = 0;
	char firstNumber = '0';
	char firstLetter = '0';
	bool isValid = false;
	int i = 0;
	int j = 0;
	char* coordinates = new char[4];
	char temp = '0';
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			if (_var.board[i][j] == 'K') // this finds the coordinates of the white king on the board
			{
				kingNumber = 8 - i;
				kingNumber += 48;
				kingLetter = j + 97; // gets his coordinates
			}
		}
	}
	coordinates[2] = kingLetter;
	coordinates[3] = kingNumber; // sets destination coordinates of king
	for (i = 0; i < 8 && !isValid; i++) // while not found a valid move to the king by enemy or when i gets to 8
	{
		firstNumber = 8 - i;
		firstNumber = firstNumber + 48;
		for (j = 0; j < 8 && !isValid; j++) // while not found a valid move to the king by enemy or when j gets to 8
		{
			firstLetter = j + 97;
			if (_var.board[i][j] >= 'a' && _var.board[i][j] <= 'z') // there's a black piece on the current index
			{
				coordinates[0] = firstLetter;
				coordinates[1] = firstNumber;
				switch (_var.board[i][j]) // goes to the piece on the board to check its movement to the king
				{
					case 'r':
						isValid = zariah.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
					case 'b':
						isValid = runner.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
					case 'n':
						isValid = parash.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
					case 'k':
						isValid = king.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
					case 'q':
						isValid = queen.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
					case 'p':
						isValid = pion.isValidMove(coordinates, 0, true); // send coordinates and true to not move
						break;
				}
			}
		}
	}
	if (isValid)
	{
		delete[] coordinates;
		return true; // there's check, isValid turned true
	}
	delete[] coordinates;
	return false; // there isnt check isValid is still false
}

bool Piece::chessCheckForBlack() // check if there's check on black player
{
	Pion pion;
	Zariah zariah;
	Queen queen;
	King king;
	Runner runner;
	Parash parash; // used to check valid movements onto the enemy king spot, will be sent a bool true value to not actually move if its a valid movement (bool notMoving variable)
	char kingNumber = 0;
	char kingLetter = 0;
	char firstNumber = '0';
	char firstLetter = '0';
	bool isValid = false;
	int i = 0;
	int j = 0;
	char* coordinates = new char[4];
	char temp = '0';
	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			if (_var.board[i][j] == 'k') // this finds the coordinates of the black king on the board
			{
				kingNumber = 8 - i;
				kingNumber += 48;
				kingLetter = j + 97; // gets his coordinates
			}
		}
	}
	coordinates[2] = kingLetter;
	coordinates[3] = kingNumber; // sets destination coordinates of king
	for (i = 0; i < 8 && !isValid; i++) // while not found a valid move to the king by enemy or when i gets to 8
	{
		firstNumber = 8 - i;
		firstNumber = firstNumber + 48;
		for (j = 0; j < 8 && !isValid; j++) // while not found a valid move to the king by enemy or when j gets to 8
		{
			firstLetter = j + 97;
			if (_var.board[i][j] >= 'A' && _var.board[i][j] <= 'Z') // there's a white piece on the current index
			{
				coordinates[0] = firstLetter;
				coordinates[1] = firstNumber;
				switch (_var.board[i][j]) // goes to the piece on the board to check its movement to the king
				{
				case 'R':
					isValid = zariah.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				case 'B':
					isValid = runner.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				case 'N':
					isValid = parash.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				case 'K':
					isValid = king.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				case 'Q':
					isValid = queen.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				case 'P':
					isValid = pion.isValidMove(coordinates, 0, true); // send coordinates and true to not move
					break;
				}
			}
		}
	}
	if (isValid)
	{
		delete[] coordinates;
		return true; // there's check, isValid turned true
	}
	delete[] coordinates;
	return false; // there isnt check isValid is still false
}
